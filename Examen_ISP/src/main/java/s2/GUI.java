package s2;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GUI {
    private static Object N = null;

    public GUI(){
        JFrame frame = new JFrame("Examen");

        JButton button = new JButton("Click");
        JTextArea area = new JTextArea();
        area.setBackground(Color.cyan);
        JTextArea area2 = new JTextArea();
        area2.setBackground(Color.yellow);

        JPanel panel = new JPanel();
        panel.setBorder(BorderFactory.createEmptyBorder(30, 30, 10, 30));
        panel.setLayout(new GridLayout(0, 1));
        panel.setBackground(Color.darkGray);
        panel.add(button);
        panel.add(area);
        panel.add(area2);

        frame.add(panel, BorderLayout.CENTER);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                N = null;
                N = area.getText().length();


            }
        });



    }

    public static void main(String[] args){
        new GUI();
    }
}
